package com.challenge.chatbot.service.ai;

import com.challenge.chatbot.domain.MessageIntents;
import com.chatbot.intent.api.IntentsApi;
import com.chatbot.intent.api.invoker.ApiClient;
import com.chatbot.intent.api.model.IntentRequest;
import com.chatbot.intent.api.model.IntentResponse;
import com.chatbot.intent.api.model.IntentResponseIntents;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UltimateAiIntentServiceTest {

    @Mock
    private IntentsApi intentsApi;
    @Mock
    private ApiClient apiClient;

    private BigDecimal confidence = new BigDecimal(0.9);
    private String intentName = "test";

    private IntentService intentService;

    @BeforeEach
    public void init() {
        intentService = new UltimateAiIntentService(intentsApi);
    }

    @Test
    public void getMessageIntentsForValidMessages() {
        IntentResponse intentResponse = getValidIntentResponse();
        when(intentsApi.getApiClient()).thenReturn(apiClient);
        when(intentsApi.intentsPost(any(IntentRequest.class))).thenReturn(intentResponse);

        Optional<MessageIntents> messageIntentsOptional = intentService.getMessageIntents("botId", "message");

        assertTrue(messageIntentsOptional.isPresent());
        MessageIntents messageIntents = messageIntentsOptional.get();

        assertEquals(messageIntentsOptional.get().getIntents().size(), intentResponse.getIntents().size());
        assertEquals(messageIntents.getIntents().get(0).getConfidence(), intentResponse.getIntents().get(0).getConfidence());
        assertEquals(messageIntents.getIntents().get(0).getName(), intentResponse.getIntents().get(0).getName());
    }

    @Test
    public void getMessageIntentsForNullValueMessagesReturnsNoIntent() {
        Optional<MessageIntents> messageIntentsOptional = intentService.getMessageIntents("botId", null);

        assertFalse(messageIntentsOptional.isPresent());
    }

    @Test
    public void getMessageIntentsForEmptyValueMessagesReturnsNoIntent() {
        Optional<MessageIntents> messageIntentsOptional = intentService.getMessageIntents("botId", "");

        assertFalse(messageIntentsOptional.isPresent());
    }

    private IntentResponse getValidIntentResponse() {
        IntentResponse intentResponse = new IntentResponse();
        IntentResponseIntents intent = new IntentResponseIntents();
        intent.setConfidence(confidence);
        intent.setName(intentName);
        intentResponse.setIntents(List.of(intent));

        return intentResponse;
    }
}