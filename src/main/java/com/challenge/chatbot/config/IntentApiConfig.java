package com.challenge.chatbot.config;

import com.chatbot.intent.api.IntentsApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class IntentApiConfig {

    @Bean
    IntentsApi getIntentsApi() {
        return new IntentsApi();
    }
}
