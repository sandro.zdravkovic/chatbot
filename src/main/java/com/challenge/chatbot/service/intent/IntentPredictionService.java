package com.challenge.chatbot.service.intent;

import com.challenge.chatbot.domain.Intent;
import com.challenge.chatbot.domain.Message;
import com.challenge.chatbot.domain.MessageIntents;
import com.challenge.chatbot.service.ai.IntentService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class IntentPredictionService {

    @Value("${intent.confidence.threshold:0.8}")
    private BigDecimal confidenceThreshold;

    private final IntentService intentService;

    public IntentPredictionService(IntentService intentService) {
        this.intentService = intentService;
    }

    public Optional<Intent> getMessageIntent(String botId, Message message) {
        Optional<MessageIntents> messageIntentsOptional = intentService.getMessageIntents(botId, message.getMessage());

        if (messageIntentsOptional.isPresent()) {
            return getHighestConfidentIntent(messageIntentsOptional.get());
        } else {
            return Optional.empty();
        }

    }

    private Optional<Intent> getHighestConfidentIntent(MessageIntents messageIntents) {
        if (CollectionUtils.isEmpty(messageIntents.getIntents())) {
            return Optional.empty();
        }

        return messageIntents.getIntents()
                .stream()
                .filter(intent -> intent.getConfidence().compareTo(confidenceThreshold) >= 0)
                .sorted()
                .findFirst();
    }
}
