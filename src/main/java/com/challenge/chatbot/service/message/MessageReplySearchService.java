package com.challenge.chatbot.service.message;

import com.challenge.chatbot.dao.model.MessageIntent;
import com.challenge.chatbot.dao.repository.IntentRepository;
import com.challenge.chatbot.domain.Message;
import com.challenge.chatbot.mapper.MessageMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MessageReplySearchService {

    private final IntentRepository intentRepository;

    @Value("${message.defaultReply}")
    private String defaultMessage;

    public MessageReplySearchService(IntentRepository intentRepository) {
        this.intentRepository = intentRepository;
    }

    public Message findByIntent(String intentName) {
        MessageIntent messageIntent = intentRepository.findByName(intentName);
        if(messageIntent == null) {
            return getDefaultMessage();
        }

        return MessageMapper.INSTANCE.toMessage(messageIntent.getReply());
    }

    private Message getDefaultMessage() {
        return new Message("AI could not give the correct answer");
    }
}
