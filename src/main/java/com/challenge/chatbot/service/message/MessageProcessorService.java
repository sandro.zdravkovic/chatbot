package com.challenge.chatbot.service.message;

import com.challenge.chatbot.domain.Intent;
import com.challenge.chatbot.domain.Message;
import com.challenge.chatbot.exception.IntentNotFoundException;
import com.challenge.chatbot.service.intent.IntentPredictionService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MessageProcessorService {

    private final IntentPredictionService intentPredictionService;
    private final MessageReplyService messageReplyService;


    public MessageProcessorService(IntentPredictionService intentPredictionService,
                                   MessageReplyService messageReplyService) {
        this.intentPredictionService = intentPredictionService;
        this.messageReplyService = messageReplyService;
    }

    public String processMessage(String botId, Message message) {
        Optional<Intent> intentOptional = intentPredictionService.getMessageIntent(botId, message);

        if (intentOptional.isPresent()) {
            return messageReplyService.getReplyMessage(intentOptional.get()).getMessage();
        } else {
            throw new IntentNotFoundException("Can not find intent for message : " + message);
        }
    }
}
