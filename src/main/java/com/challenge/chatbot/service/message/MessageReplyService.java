package com.challenge.chatbot.service.message;

import com.challenge.chatbot.domain.Intent;
import com.challenge.chatbot.domain.Message;
import org.springframework.stereotype.Service;

@Service
public class MessageReplyService {

    private final MessageReplySearchService messageReplySearchService;

    public MessageReplyService(MessageReplySearchService messageReplySearchService) {
        this.messageReplySearchService = messageReplySearchService;
    }

    public Message getReplyMessage(Intent intent) {
        return messageReplySearchService.findByIntent(intent.getName());
    }
}
