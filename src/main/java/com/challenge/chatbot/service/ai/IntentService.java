package com.challenge.chatbot.service.ai;

import com.challenge.chatbot.domain.MessageIntents;

import java.util.Optional;

public interface IntentService {

    Optional<MessageIntents> getMessageIntents(String botId, String message);
}
