package com.challenge.chatbot.service.ai;

import com.challenge.chatbot.domain.MessageIntents;
import com.challenge.chatbot.mapper.IntentResponseMapper;
import com.chatbot.intent.api.IntentsApi;
import com.chatbot.intent.api.model.IntentRequest;
import com.chatbot.intent.api.model.IntentResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Optional;

@Service
public class UltimateAiIntentService implements IntentService {

    private static final String AUTHORIZATION_HEADER = "authorization";
    private final IntentsApi intentsApi;

    @Value("${ultimate.ai.api.header}")
    private String apiAuthorization;

    public UltimateAiIntentService(IntentsApi intentsApi) {
        this.intentsApi = intentsApi;
    }


    @Override
    public Optional<MessageIntents> getMessageIntents(String botId, String message) {
        if (!StringUtils.hasText(message)) {
            return Optional.empty();
        }

        IntentRequest intentRequest = new IntentRequest();
        intentRequest.setBotId(botId);
        intentRequest.setMessage(message);

        intentsApi.getApiClient().addDefaultHeader(AUTHORIZATION_HEADER, apiAuthorization);
        IntentResponse intentResponse = intentsApi.intentsPost(intentRequest);
        MessageIntents messageIntents = IntentResponseMapper.INSTANCE.toMessageIntents(intentResponse);
        return Optional.ofNullable(messageIntents);
    }
}
