package com.challenge.chatbot.controller;

import com.challenge.chatbot.domain.Message;
import com.challenge.chatbot.dto.MessageDto;
import com.challenge.chatbot.exception.IntentNotFoundException;
import com.challenge.chatbot.mapper.MessageDtoMapper;
import com.challenge.chatbot.service.message.MessageProcessorService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController("/messages")
public class MessageController {

    private final MessageProcessorService messageSenderService;

    public MessageController(MessageProcessorService messageSenderService) {
        this.messageSenderService = messageSenderService;
    }

    @PostMapping("/{botId}/send-message")
    public String sendMessage(@PathVariable("botId") String botId, @RequestBody MessageDto messageDto) {
        Message message = MessageDtoMapper.INSTANCE.toMessage(messageDto);

        try {
            return messageSenderService.processMessage(botId, message);
        } catch (IntentNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Message not recognized", e);
        }
    }
}
