package com.challenge.chatbot.mapper;

import com.challenge.chatbot.domain.Message;
import com.challenge.chatbot.dto.MessageDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MessageDtoMapper {

    MessageDtoMapper INSTANCE = Mappers.getMapper(MessageDtoMapper.class);

    Message toMessage(MessageDto messageDto);
}
