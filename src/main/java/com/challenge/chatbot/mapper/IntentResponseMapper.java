package com.challenge.chatbot.mapper;

import com.challenge.chatbot.domain.MessageIntents;
import com.chatbot.intent.api.model.IntentResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface IntentResponseMapper {

    IntentResponseMapper INSTANCE = Mappers.getMapper(IntentResponseMapper.class);

    MessageIntents toMessageIntents(IntentResponse intentResponse);
}
