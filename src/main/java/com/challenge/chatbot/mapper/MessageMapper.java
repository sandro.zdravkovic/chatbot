package com.challenge.chatbot.mapper;

import com.challenge.chatbot.dao.model.IntentReply;
import com.challenge.chatbot.domain.Message;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MessageMapper {

    MessageMapper INSTANCE = Mappers.getMapper(MessageMapper.class);

    @Mapping(target = "message", source = "text")
    Message toMessage(IntentReply reply);
}
