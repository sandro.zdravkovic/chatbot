package com.challenge.chatbot.dao.model;

import lombok.Data;

import java.util.List;

@Data
public class TrainingData {

    private List<TrainingMessage> messages;
}
