package com.challenge.chatbot.dao.model;

import lombok.Data;

@Data
public class TrainingMessage {

    private String id;
    private String text;
}
