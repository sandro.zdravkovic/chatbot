package com.challenge.chatbot.dao.model;

import lombok.Data;

@Data
public class IntentReply {

    private String id;
    private String text;
}
