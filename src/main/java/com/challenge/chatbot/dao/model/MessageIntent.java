package com.challenge.chatbot.dao.model;

import lombok.Data;
import nonapi.io.github.classgraph.json.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "messageIntent")
public class MessageIntent {

    @Id
    private String id;
    private String name;
    private String description;
    private TrainingData trainingData;
    private IntentReply reply;
}
