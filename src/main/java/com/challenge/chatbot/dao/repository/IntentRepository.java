package com.challenge.chatbot.dao.repository;

import com.challenge.chatbot.dao.model.MessageIntent;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IntentRepository extends MongoRepository<MessageIntent, String> {

    MessageIntent findByName(String name);
}
