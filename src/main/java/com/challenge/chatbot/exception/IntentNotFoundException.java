package com.challenge.chatbot.exception;

public class IntentNotFoundException extends RuntimeException {

    public IntentNotFoundException(String message) {
        super(message);
    }
}
