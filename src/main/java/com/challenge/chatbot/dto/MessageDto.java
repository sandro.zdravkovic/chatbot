package com.challenge.chatbot.dto;

import lombok.Data;

@Data
public class MessageDto {

    private String message;
}
