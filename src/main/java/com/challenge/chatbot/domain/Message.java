package com.challenge.chatbot.domain;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Message {

    @NonNull
    private String message;
}
