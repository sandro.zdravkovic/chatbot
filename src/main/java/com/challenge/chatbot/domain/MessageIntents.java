package com.challenge.chatbot.domain;

import lombok.Data;

import java.util.List;

@Data
public class MessageIntents {

    private List<Intent> intents;
}
