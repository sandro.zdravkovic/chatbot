package com.challenge.chatbot.domain;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Intent implements Comparable<Intent> {

    private BigDecimal confidence;
    private String name;

    @Override
    public int compareTo(Intent intent) {
        return intent.getConfidence().compareTo(this.confidence);
    }
}
