

# Message


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**botId** | **String** |  | 
**message** | **String** | The message to analyze for intent | 



