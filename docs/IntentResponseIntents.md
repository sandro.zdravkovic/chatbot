

# IntentResponseIntents


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**confidence** | **BigDecimal** |  |  [optional]
**name** | **String** |  |  [optional]



