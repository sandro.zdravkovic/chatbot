

# IntentResponse


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**intents** | [**List&lt;IntentResponseIntents&gt;**](IntentResponseIntents.md) |  |  [optional]
**entities** | **List&lt;Object&gt;** |  |  [optional]



