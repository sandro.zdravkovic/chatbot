

# InlineResponse200


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**intents** | [**List&lt;InlineResponse200Intents&gt;**](InlineResponse200Intents.md) |  |  [optional]
**entities** | **List&lt;Object&gt;** |  |  [optional]



