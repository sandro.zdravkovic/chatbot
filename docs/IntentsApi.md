# IntentsApi

All URIs are relative to *https://chat.ultimate.ai/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**intentsPost**](IntentsApi.md#intentsPost) | **POST** /intents | Get predicted intents for a visitor message and bot



## intentsPost

> IntentResponse intentsPost(intentRequest)

Get predicted intents for a visitor message and bot

### Example

```java
// Import classes:
import com.chatbot.intent.api.invoker.ApiClient;
import com.chatbot.intent.api.invoker.ApiException;
import com.chatbot.intent.api.invoker.Configuration;
import com.chatbot.intent.api.invoker.auth.*;
import com.chatbot.intent.api.invoker.models.*;
import com.chatbot.intent.api.IntentsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://chat.ultimate.ai/api");
        
        // Configure API key authorization: ApiKeyAuth
        ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
        ApiKeyAuth.setApiKey("YOUR API KEY");
        // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
        //ApiKeyAuth.setApiKeyPrefix("Token");

        IntentsApi apiInstance = new IntentsApi(defaultClient);
        IntentRequest intentRequest = new IntentRequest(); // IntentRequest | 
        try {
            IntentResponse result = apiInstance.intentsPost(intentRequest);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling IntentsApi#intentsPost");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **intentRequest** | [**IntentRequest**](IntentRequest.md)|  |

### Return type

[**IntentResponse**](IntentResponse.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **400** | Bad Request |  -  |
| **401** | Unauthorized |  -  |
| **404** | Bot not recognized |  -  |
| **500** | Internal Server Error |  -  |

